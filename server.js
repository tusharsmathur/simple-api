const http = require('http')
const fs = require("fs")

const Data = fs.readFileSync("data.json")

const srv = http.createServer((req, res) => {
    
    const path = req.url
    switch (path) {
        case'/':
        case '/home': res.end(Data); break;
        case '/movie': res.end("Movie"); break;
        case '/cricket': res.end("Cricket"); break;
        case '/update': {
            let data = []
            req.on("data", (line) => {
                data.push(line)
            })
            req.on("end", () => {

                data = JSON.parse(data)

                const d = JSON.parse(Data)

                d.push(data)

                fs.writeFile("data.json", JSON.stringify(d), (err) => {
                    if (err) {
                        res.writeHead(500)
                        res.end(err)
                    }
                })
                res.end("Success")
            })
            break;
        }
        default: {
            res.writeHead(404)
            res.end("Page not found")
        }
    }
})

srv.listen(5000, "127.0.0.1", () => {
    console.log("Server started at port 5000")
})